package net.dabler.pageloader.sourceView;

public class HttpException extends Exception {

    public HttpException(int errorCode) {
        super(String.valueOf(errorCode));
    }
}
