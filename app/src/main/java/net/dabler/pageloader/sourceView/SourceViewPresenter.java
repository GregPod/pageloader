package net.dabler.pageloader.sourceView;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.io.IOException;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SourceViewPresenter extends MvpBasePresenter<SourceViewInterfaces.View> {

    OkHttpClient okHttpClient;

    @Inject
    public SourceViewPresenter(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    void getPageSource(String url) {
        getReponseFrom(okHttpClient, url)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnComplete(() -> {
                    if (isViewAttached()) {
                        getView().setProgressVisible(false);
                    }
                })
                .subscribe(responseBody -> {
                    if (isViewAttached()) {
                        getView().setResponseBody(responseBody);
                    }
                }, error -> {
                    dispatchHttpErrors(error);
                });
    }

    private Observable<String> getReponseFrom(OkHttpClient client, String url) {
        return Observable.defer(() -> {
            try {
                Response response = client.newCall(new Request.Builder().url(url).build()).execute();
                if (response.isSuccessful()) {
                    return Observable.just(response.body().string());
                } else {
                    return Observable.error(new HttpException(response.code()));
                }
            } catch (IOException e) {
                return Observable.error(e);
            }
        });
    }

    private void dispatchHttpErrors(Throwable exception) {
        if (isViewAttached()) {
            switch (exception.getMessage()) {
                case "400":
                    getView().showBadRequestError();
                case "401":
                    getView().showUnauthorizedError();
                case "403":
                    getView().showForbiddenError();
                case "404":
                    getView().showPageNotFoundError();
                case "500":
                    getView().showInternalServerError();
                default:
                    getView().showDefaultHttpError(exception.getMessage());
            }
        }
    }
}
