package net.dabler.pageloader.sourceView;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import net.dabler.pageloader.App;
import net.dabler.pageloader.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SourceViewFragment extends MvpFragment<SourceViewInterfaces.View, SourceViewPresenter>
        implements SourceViewInterfaces.View {

    private static final String URL_PARAM = "URL_PARAM";

    @BindView(R.id.source_text_view)
    TextView sourceTextView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    private OnFragmentInteractionListener listener;

    @Inject
    SourceViewPresenter presenter;

    public SourceViewFragment() {
        // Required empty public constructor
    }

    @Override
    public SourceViewPresenter createPresenter() {
        return presenter;
    }

    public static SourceViewFragment newInstance(String url) {
        SourceViewFragment fragment = new SourceViewFragment();
        Bundle args = new Bundle();
        args.putString(URL_PARAM, url);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_source_view, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getArguments() != null) {
            presenter.getPageSource(getArguments().getString(URL_PARAM));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    public void setResponseBody(String body) {
        sourceTextView.setText(body);
    }

    @Override
    public void setProgressVisible(boolean visible) {
        progressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPageNotFoundError() {
        showErrorAndGoBack(R.string.page_not_found_error);
    }

    @Override
    public void showDefaultHttpError(String message) {
        showErrorAndGoBack(R.string.default_error);
    }

    @Override
    public void showBadRequestError() {
        showErrorAndGoBack(R.string.page_not_found_error);
    }

    @Override
    public void showUnauthorizedError() {
        showErrorAndGoBack(R.string.unauthorized_error);
    }

    @Override
    public void showForbiddenError() {
        showErrorAndGoBack(R.string.forbidden_page_error);
    }

    @Override
    public void showInternalServerError() {
        showErrorAndGoBack(R.string.internal_server_error);
    }

    public interface OnFragmentInteractionListener {
        void goToAddressInputFragment();
    }

    private void showErrorAndGoBack(int errorStringResource) {
        Toast.makeText(getContext(), errorStringResource, Toast.LENGTH_LONG).show();
        listener.goToAddressInputFragment();
    }
}
