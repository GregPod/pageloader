package net.dabler.pageloader.sourceView;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface SourceViewInterfaces {

    interface View extends MvpView {

        void setResponseBody(String body);

        void setProgressVisible(boolean visible);

        void showPageNotFoundError();

        void showDefaultHttpError(String message);

        void showBadRequestError();

        void showUnauthorizedError();

        void showForbiddenError();

        void showInternalServerError();
    }
}
