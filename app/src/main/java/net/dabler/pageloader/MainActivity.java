package net.dabler.pageloader;

import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import net.dabler.pageloader.addressInput.AddressInputFragment;
import net.dabler.pageloader.sourceView.SourceViewFragment;

public class MainActivity extends AppCompatActivity implements AddressInputFragment.OnFragmentInteractionListener, SourceViewFragment.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, AddressInputFragment.newInstance())
                .commit();
    }

    @Override
    public void openSourceView(String url) {
        getSupportFragmentManager().beginTransaction().addToBackStack("").replace(R.id.fragment_container, SourceViewFragment.newInstance(url)).commit();
    }

    @Override
    public void goToAddressInputFragment() {
        getSupportFragmentManager().popBackStack();
    }
}
