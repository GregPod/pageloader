package net.dabler.pageloader.addressInput;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import javax.inject.Inject;

public class AddressInputPresenter extends MvpBasePresenter<AddressInputInterfaces.View> {

    @Inject
    public AddressInputPresenter() {
        //no-op
    }

    public void onAddressEntered(String url) {
        if (isViewAttached()) {
            if (url.contentEquals("")) {
                getView().showEmptyUrlError();
            } else if (!(android.util.Patterns.WEB_URL.matcher(url).matches() && (url.startsWith("http")))) {
                getView().showWrongUrlFormatError();
            } else {
                getView().openSourceView(url);
            }
        }
    }
}
