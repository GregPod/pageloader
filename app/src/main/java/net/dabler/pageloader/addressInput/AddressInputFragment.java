package net.dabler.pageloader.addressInput;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

import net.dabler.pageloader.App;
import net.dabler.pageloader.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressInputFragment extends MvpFragment<AddressInputInterfaces.View, AddressInputPresenter>
        implements AddressInputInterfaces.View {

    private OnFragmentInteractionListener listener;

    @BindView(R.id.url_edit_text)
    EditText urlEditText;

    @Inject
    AddressInputPresenter presenter;

    public AddressInputFragment() {
        // Required empty public constructor
    }

    @Override
    public AddressInputPresenter createPresenter() {
        return presenter;
    }

    public static AddressInputFragment newInstance() {
        return new AddressInputFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address_input, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.open_source_button)
    public void onButtonPressed() {
        presenter.onAddressEntered(urlEditText.getText().toString());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void openSourceView(String url){
        if (listener != null) {
            listener.openSourceView(url);
        }
    }

    @Override
    public void showEmptyUrlError() {
        Toast.makeText(getContext(), R.string.entered_url_empty_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showWrongUrlFormatError() {
        Toast.makeText(getContext(), R.string.entered_url_wrong_format_error, Toast.LENGTH_SHORT).show();
    }

    public interface OnFragmentInteractionListener {

        void openSourceView(String url);
    }
}
