package net.dabler.pageloader.addressInput;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface AddressInputInterfaces {

    interface View extends MvpView {
        void openSourceView(String url);

        void showEmptyUrlError();

        void showWrongUrlFormatError();
    }
}
